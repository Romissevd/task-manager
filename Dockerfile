FROM python:3.12

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV HOME=/usr/src
ENV APP_HOME=/usr/src/app
#ENV MEDIA_ROOT=/mnt/task_manager
RUN mkdir $APP_HOME
#RUN mkdir $MEDIA_ROOT
#RUN mkdir $MEDIA_ROOT/static
#RUN mkdir $MEDIA_ROOT/media
WORKDIR $APP_HOME

COPY requirements.txt .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /usr/src/app
