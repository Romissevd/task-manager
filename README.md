# Task Manager

### Description
This is a simple task manager that allows you to create, edit, and delete tasks.
It also allows you to mark tasks as complete or incomplete. The tasks are stored in a database and are displayed
in a table. The table can be sorted by any of the columns. The tasks can also be filtered by the status of the task.

* Company

Any user can create a company. A company can have many users and a user can have many companies. Company can invite
users. The user can switch between his companies. Each company has its own projects and tasks.

### Software Requirements

* [Python 3.12](https://www.python.org/downloads/)
* [Django 5.0.1](https://www.djangoproject.com/download/)
* [Django Rest Framework](https://www.django-rest-framework.org/)
* [PostgreSQL](https://www.postgresql.org/download/)
* [Celery 5.1.2](https://docs.celeryproject.org/en/stable/getting-started/introduction.html)
* [RabbitMQ](https://www.rabbitmq.com/download.html)

### Development

Pre-commit hooks are used to ensure that the code is formatted correctly and that the tests pass before
a commit is made.

```bash
pre-commit install
```

### Running the Application

Create a file called `.env` in the root directory of the project. Add the necessary environment variables based on
the env.example file
