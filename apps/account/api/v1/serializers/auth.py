from django.contrib.auth import authenticate
from rest_framework import serializers

from apps.core.models import UserManager
from main.constants import DEFAULT_ERROR_MESSAGES
from main.settings import PASSWORD_MIN_LENGTH


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(error_messages=DEFAULT_ERROR_MESSAGES)
    password = serializers.CharField(
        min_length=PASSWORD_MIN_LENGTH,
        error_messages=DEFAULT_ERROR_MESSAGES,
    )

    def validate_email(self, value):
        return UserManager.normalize_email(value)

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return {'user': user}
        raise serializers.ValidationError('Incorrect credentials')
