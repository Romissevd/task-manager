from datetime import timedelta

from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError as DjangoValidationError
from django.utils import timezone
from rest_framework import serializers

from apps.account.models import UserRegistration
from apps.company.models import InvitePersonToCompany
from apps.core.models import User


class UserRegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    # recaptcha = ...  TODO: add recaptcha field

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError('User with such email already exists.')
        if UserRegistration.objects.filter(
                email=value,
                created_at__gte=timezone.now() - timedelta(minutes=5),
        ).exists():
            raise serializers.ValidationError('Message send. Check your email or try again later.')
        return value


class ConfirmUserRegistrationSerializer(serializers.Serializer):
    uuid = serializers.UUIDField(required=True)
    password = serializers.CharField(required=True, min_length=8, max_length=128)
    confirm_password = serializers.CharField(required=True, min_length=8, max_length=128)

    def validate_uuid(self, value):
        if not UserRegistration.objects.filter(registration_uuid=value, user__isnull=True).exists():
            raise serializers.ValidationError('Invalid token or user was created.')
        return value

    def validate_password(self, value):
        try:
            password_validation.validate_password(value)
        except DjangoValidationError as exc:
            raise serializers.ValidationError(exc.messages)
        return value

    def validate(self, attrs):
        if attrs.get('password') != attrs.get('confirm_password'):
            raise serializers.ValidationError('Passwords do not match.')
        return attrs


class RegistrationByCompanyInvitationSerializer(serializers.Serializer):
    uuid = serializers.UUIDField(required=True)
    password = serializers.CharField(required=False, min_length=8, max_length=128)
    confirm_password = serializers.CharField(required=False, min_length=8, max_length=128)

    def validate_uuid(self, value):
        if not InvitePersonToCompany.objects.filter(uuid=value, date_of_joining__isnull=True).exists():
            raise serializers.ValidationError('Invalid token.')
        return value

    def validate_password(self, value):
        try:
            password_validation.validate_password(value)
        except DjangoValidationError as exc:
            raise serializers.ValidationError(exc.messages)
        return value

    def validate(self, attrs):
        uuid = attrs.get('uuid')
        password = attrs.get('password')
        confirm_password = attrs.get('confirm_password')
        invite = InvitePersonToCompany.objects.filter(uuid=uuid).first()
        if not invite or invite.date_of_joining:
            raise serializers.ValidationError({'uuid': 'Invalid token.'})
        user_was_registered = User.objects.filter(email=invite.email).exists()
        if not user_was_registered and not password:
            raise serializers.ValidationError({'password': 'This field is required.'})
        if password and confirm_password and password != confirm_password:
            raise serializers.ValidationError('Passwords do not match.')
        attrs['invite'] = invite
        return attrs
