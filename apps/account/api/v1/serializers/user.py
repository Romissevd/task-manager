from rest_framework import serializers

from apps.core.models import User


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'email', 'first_name', 'last_name', 'middle_name',
        )
        read_only_fields = ('email',)
