from django.urls import path

from apps.account.api.v1.views.auth import LoginView
from apps.account.api.v1.views.registration import (
    ConfirmUserRegistrationView,
    RegistrationByCompanyInvitationView,
    UserRegistrationView,
)
from apps.account.api.v1.views.user import UserInfoView

urlpatterns = [
    path('registration/', UserRegistrationView.as_view(), name='user-registration'),
    path('confirm_registration/', ConfirmUserRegistrationView.as_view(), name='confirm-user-registration'),
    path(
        'registration_by_company_invitation/',
        RegistrationByCompanyInvitationView.as_view(),
        name='registration-by-company-invitation',
    ),
    path('login/', LoginView.as_view(), name='login'),
    path('me/', UserInfoView.as_view(), name='user-info'),
]
