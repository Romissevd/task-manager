from drf_spectacular.utils import extend_schema
from rest_framework import (
    permissions,
    status,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.account.api.v1.serializers.auth import LoginSerializer
from apps.account.services.user import UserLoginService


class LoginView(APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = LoginSerializer

    @extend_schema(
        methods=['POST'],
        description='Login user via email.',
        responses={status.HTTP_201_CREATED: {'example': {'token': '428262fff8efd85e3faf8093f90420199d4b1ce5'}}},
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token = UserLoginService.get_new_user_token(user)
        return Response({'token': token}, status=status.HTTP_201_CREATED)
