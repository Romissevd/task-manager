from drf_spectacular.utils import extend_schema
from rest_framework import (
    generics,
    permissions,
    status,
)
from rest_framework.response import Response

from apps.account.api.v1.serializers.registration import (
    ConfirmUserRegistrationSerializer,
    RegistrationByCompanyInvitationSerializer,
    UserRegistrationSerializer,
)
from apps.account.services.user import (
    UserCreatorService,
    UserLoginService,
)

SUCCESS_REGISTRATION_MESSAGE = {'status': 'success'}


class UserRegistrationView(generics.GenericAPIView):
    serializer_class = UserRegistrationSerializer
    permission_classes = (permissions.AllowAny,)

    @extend_schema(
        methods=['POST'],
        description='Registration user via email.',
        request=UserRegistrationSerializer,
        responses={status.HTTP_201_CREATED: {'example': SUCCESS_REGISTRATION_MESSAGE}},
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserCreatorService.send_email_confirming_registration(
            request=request, email=serializer.validated_data.get('email'),
        )
        return Response(SUCCESS_REGISTRATION_MESSAGE, status=status.HTTP_201_CREATED)


class ConfirmUserRegistrationView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ConfirmUserRegistrationSerializer

    @extend_schema(
        methods=['GET'],
        description='Confirm registration user via email.',
        responses={status.HTTP_201_CREATED: {'example': {'token': '428262fff8efd85e3faf8093f90420199d4b1ce5'}}},
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        user = UserCreatorService.confirm_registration(
            uuid=validated_data.get('uuid'),
            password=validated_data.get('password'),
        )
        token = UserLoginService.get_new_user_token(user)
        return Response({'token': token}, status=status.HTTP_201_CREATED)


class RegistrationByCompanyInvitationView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = RegistrationByCompanyInvitationSerializer

    @extend_schema(
        methods=['GET'],
        description='Confirm registration user by company invitation',
        responses={status.HTTP_201_CREATED: {'example': {'token': '428262fff8efd85e3faf8093f90420199d4b1ce5'}}},
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        invite = serializer.validated_data.get('invite')
        user = UserCreatorService.confirm_registration_by_invitation(
            invite=invite,
            password=validated_data.get('password'),
        )
        token = UserLoginService.get_new_user_token(user)
        return Response({'token': token}, status=status.HTTP_201_CREATED)
