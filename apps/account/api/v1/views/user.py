from rest_framework import (
    generics,
    permissions,
)

from apps.account.api.v1.serializers.user import UserInfoSerializer


class UserInfoView(generics.RetrieveUpdateAPIView, generics.UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserInfoSerializer

    def get_object(self):
        return self.request.user
