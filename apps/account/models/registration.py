import uuid

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _


class UserRegistration(models.Model):
    email = models.EmailField()
    registration_uuid = models.UUIDField(blank=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    user = models.OneToOneField('core.User', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = _('User registration')
        verbose_name_plural = _('Users registration')
