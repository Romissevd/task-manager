import uuid

from django.core.exceptions import ValidationError as DjangoValidationError
from django.core.validators import URLValidator
from django.utils import timezone
from rest_framework.authtoken.models import Token

from apps.account.models import UserRegistration
from apps.company.models import (
    InvitePersonToCompany,
    PersonInCompany,
)
from apps.core.models import (
    User,
    UserProfile,
)
from apps.notification.tasks import send_email


class UserCreatorService:

    @staticmethod
    def send_email_confirming_registration(
        request,
        email: str,
        link_url: str = '/registration/email-confirmation',  # This is the url to which the user will be redirected
    ):
        template_name = 'email_confirming_registration.html'
        email_uuid = uuid.uuid4()
        email_link = f'{link_url}?uid={email_uuid}'
        try:
            validate = URLValidator()
            validate(email_link)
        except DjangoValidationError:
            email_link = request.build_absolute_uri(email_link)
        UserRegistration.objects.update_or_create(
            email=email, defaults={'registration_uuid': email_uuid, 'created_at': timezone.now()},
        )
        send_email.delay(
            recipients=[email],
            title='Registration confirmation',
            template_name=template_name,
            data_for_template={'link': email_link},
        )

    @staticmethod
    def confirm_registration(uuid: uuid.UUID, password: str) -> User:
        user_registration = UserRegistration.objects.get(registration_uuid=uuid, user__isnull=True)
        user = User.objects.create_user(
            email=user_registration.email,
            password=password,
        )
        user_registration.user = user
        user_registration.save()
        return user

    @staticmethod
    def confirm_registration_by_invitation(invite: InvitePersonToCompany, password: str | None) -> User:
        user = User.objects.filter(email=invite.email).first()
        if not user:
            user = User.objects.create_user(email=invite.email, password=password)
        person_in_company = PersonInCompany.objects.create(person=user, company=invite.company, role=invite.role)
        if hasattr(user, 'profile'):
            user.profile.current_person_in_company = person_in_company
            user.profile.save()
        else:
            UserProfile.objects.create(user=user, current_person_in_company=person_in_company)
        invite.date_of_joining = timezone.now()
        invite.save()
        return user


class UserLoginService:

    @staticmethod
    def get_new_user_token(user: User) -> str:
        Token.objects.filter(user=user).delete()
        token = Token.objects.create(user=user)
        return token.key
