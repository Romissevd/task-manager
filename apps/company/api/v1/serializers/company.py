from rest_framework import serializers

from apps.company.exceptions import CompanyNotExistsError
from apps.company.models import PersonInCompany
from apps.company.models.company import Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('uuid', 'name', 'logo', 'description',  'created_at', 'updated_at')
        read_only_fields = ('uuid', 'created_at', 'updated_at', 'logo')


class ListUserCompaniesSerializer(serializers.ModelSerializer):
    is_available = serializers.BooleanField()

    class Meta:
        model = Company
        fields = ('uuid', 'name', 'logo', 'is_available')
        read_only_fields = fields


class UploadCompanyLogoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('logo',)


class ChangeCurrentCompanySerializer(serializers.Serializer):
    uuid = serializers.UUIDField()

    def validate(self, attrs):
        request = self.context['request']
        uuid = attrs.get('uuid')
        company = Company.objects.filter(uuid=uuid).first()
        if not company:
            raise CompanyNotExistsError()
        person_in_company = PersonInCompany.objects.filter(company=company, person=request.user).first()
        if not person_in_company:
            raise serializers.ValidationError('You are not a member of this company')
        if not person_in_company.is_active:
            raise serializers.ValidationError('You are not active member of this company')
        attrs['person_in_company'] = person_in_company
        return attrs
