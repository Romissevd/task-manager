from rest_framework import serializers

from apps.company.models import (
    InvitePersonToCompany,
    PersonInCompany,
)


class PersonInCompanySerializer(serializers.ModelSerializer):
    person = serializers.ReadOnlyField(source='person.full_name')

    class Meta:
        model = PersonInCompany
        fields = ('id', 'person', 'role', 'is_active', 'created_at')
        read_only_fields = ('created_at',)

    def validate(self, attrs):
        user = self.context['request'].user
        author_person_in_company = user.profile.current_person_in_company
        is_owner_company = author_person_in_company.role == PersonInCompany.CompanyRole.OWNER
        role = attrs.get('role')
        is_active = attrs.get('is_active')
        if (
                role and
                role in (PersonInCompany.CompanyRole.OWNER, PersonInCompany.CompanyRole.ADMIN) and
                not is_owner_company
        ):
            raise serializers.ValidationError('Only company owner can set admin role')
        if self.instance and is_active is False:
            if self.instance.person == user:
                raise serializers.ValidationError('You can not deactivate yourself')
            if self.instance.role == PersonInCompany.CompanyRole.OWNER and not is_owner_company:
                raise serializers.ValidationError('Only company owner can deactivate owner')
        return attrs


class InvitePersonToCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = InvitePersonToCompany
        fields = ('uuid', 'email', 'role', 'is_active', 'created_at', 'date_of_joining')
        read_only_fields = ('uuid', 'created_at', 'date_of_joining')

    def validate(self, attrs):
        email = attrs.get('email')
        user = self.context['request'].user
        author_person_in_company = user.profile.current_person_in_company
        is_owner_company = author_person_in_company.role == PersonInCompany.CompanyRole.OWNER
        role = attrs.get('role')
        if (
                role and
                role in (PersonInCompany.CompanyRole.OWNER, PersonInCompany.CompanyRole.ADMIN) and
                not is_owner_company
        ):
            raise serializers.ValidationError('Only company owner can set admin role')
        if email:
            if PersonInCompany.objects.filter(person__email=email, company=author_person_in_company.company).exists():
                raise serializers.ValidationError('User with this email address is already registered with the company')
            if InvitePersonToCompany.objects.filter(email=email, company=author_person_in_company.company).exists():
                raise serializers.ValidationError('Invite with this email address is already exists')
        return attrs
