from django.urls import path
from rest_framework import routers

from apps.company.api.v1.views.company import (
    ChangeCurrentCompanyView,
    CompanyView,
    UploadCompanyLogoView,
    UserCompaniesView,
)
from apps.company.api.v1.views.person_in_company import (
    InvitePersonToCompanyViewSet,
    PersonInCompanyViewSet,
)

router = routers.SimpleRouter()
router.register('persons', PersonInCompanyViewSet, basename='persons-in-company')
router.register('invite_person', InvitePersonToCompanyViewSet, basename='invite-person-in-company')

urlpatterns = [
    path('current/', CompanyView.as_view(), name='current-company'),
    path('my/', UserCompaniesView.as_view(), name='all-user-companies'),
    path('change_current/', ChangeCurrentCompanyView.as_view(), name='change-current-company'),
    path('upload_logo/', UploadCompanyLogoView.as_view(), name='upload-company-logo'),
]

urlpatterns += router.urls
