from django.db.models import (
    Case,
    When,
)
from rest_framework import (
    generics,
    parsers,
    permissions,
    status,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.company.api.v1.serializers.company import (
    ChangeCurrentCompanySerializer,
    CompanySerializer,
    ListUserCompaniesSerializer,
    UploadCompanyLogoSerializer,
)
from apps.company.exceptions import CompanyNotExistsError
from apps.company.models.company import Company
from apps.company.models.person_in_company import PersonInCompany
from apps.company.permissions import (
    CompanyPermissions,
    UploadCompanyLogoPermissions,
)


class CompanyView(
    generics.CreateAPIView,
    generics.RetrieveAPIView,
    generics.UpdateAPIView,
    generics.DestroyAPIView,
):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = (permissions.IsAuthenticated, CompanyPermissions)

    def get_object(self):
        company = self.request.user.profile.current_person_in_company.company
        if not company:
            raise CompanyNotExistsError()
        self.check_object_permissions(self.request, company)
        return company

    def perform_create(self, serializer):
        user = self.request.user
        company = serializer.save(author=user)
        person_in_company = PersonInCompany.objects.create(
            company=company,
            person=user,
            role=PersonInCompany.CompanyRole.OWNER,
        )
        user.profile.current_person_in_company = person_in_company
        user.profile.save(update_fields=('current_person_in_company',))


class UserCompaniesView(generics.ListAPIView):
    queryset = Company.objects.all()
    serializer_class = ListUserCompaniesSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(
            persons__person=self.request.user,
        ).annotate(
            is_available=Case(
                When(persons__is_active=True, then=True),
                default=False,
            ),
        ).order_by(
            '-is_available',
        )


class UploadCompanyLogoView(generics.GenericAPIView):
    queryset = Company.objects.all()
    serializer_class = UploadCompanyLogoSerializer
    permission_classes = (permissions.IsAuthenticated, UploadCompanyLogoPermissions)
    parser_classes = (parsers.FormParser, parsers.MultiPartParser)

    def get_object(self):
        company = getattr(self.request.user, 'company', None)
        if not company:
            raise CompanyNotExistsError()
        return company

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        company = self.get_object()
        company.logo = serializer.validated_data.get('logo')
        company.save()
        return Response({'status': 'success'}, status=status.HTTP_201_CREATED)


class ChangeCurrentCompanyView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ChangeCurrentCompanySerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        person_in_company = serializer.validated_data.get('person_in_company')
        request.user.profile.current_person_in_company = person_in_company
        request.user.profile.save(update_fields=('current_person_in_company',))
        return Response({'status': 'success'}, status=status.HTTP_201_CREATED)
