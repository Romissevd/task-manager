from rest_framework import (
    mixins,
    permissions,
    viewsets,
)

from apps.company.api.v1.serializers.person_in_company import (
    InvitePersonToCompanySerializer,
    PersonInCompanySerializer,
)
from apps.company.models import (
    InvitePersonToCompany,
    PersonInCompany,
)
from apps.company.permissions import (
    InvitePersonToCompanyPermissions,
    PersonInCompanyPermissions,
)
from apps.company.tasks.company_notifications import send_invite_to_user_to_register_with_company


class PersonInCompanyViewSet(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = PersonInCompany.objects.select_related('person').all()
    serializer_class = PersonInCompanySerializer
    permission_classes = (permissions.IsAuthenticated, PersonInCompanyPermissions)

    def get_queryset(self):
        request_person_in_company = self.request.user.profile.current_person_in_company
        queryset = self.queryset.filter(company=request_person_in_company.company)
        if request_person_in_company.role == PersonInCompany.CompanyRole.EMPLOYEE:
            queryset = queryset.filter(is_active=True)
        return queryset


class InvitePersonToCompanyViewSet(viewsets.ModelViewSet):
    queryset = InvitePersonToCompany.objects.all()
    serializer_class = InvitePersonToCompanySerializer
    permission_classes = (permissions.IsAuthenticated, InvitePersonToCompanyPermissions)

    def get_queryset(self):
        return self.queryset.filter(company=self.request.user.profile.current_person_in_company.company)

    def perform_create(self, serializer):
        invite = serializer.save(company=self.request.user.profile.current_person_in_company.company)
        domain = self.request.build_absolute_uri('/')[:-1]
        send_invite_to_user_to_register_with_company.delay(invite.uuid, domain)
