from rest_framework import exceptions


class CompanyNotExistsError(exceptions.NotFound):
    default_detail = 'Company not found'
