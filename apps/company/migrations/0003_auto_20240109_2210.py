# Generated by Django 5.0.1 on 2024-01-09 22:10

from django.db import migrations


def set_current_person_in_company(apps, schema_editor):
    PersonInCompany = apps.get_model('company', 'PersonInCompany')
    Company = apps.get_model('company', 'Company')

    companies_without_person_in_company = Company.objects.filter(persons__isnull=True).all()
    for company in companies_without_person_in_company:
        person = company.author
        person_in_company = PersonInCompany.objects.create(
            company=company, person=person, role='owner',
        )
        person.profile.current_person_in_company = person_in_company
        person.profile.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_userprofile'),
        ('company', '0002_personincompany'),
    ]

    operations = [
        migrations.RunPython(set_current_person_in_company, migrations.RunPython.noop),
    ]
