import uuid

from django.db import models

from mixins.models import DateTimesABC


class Company(DateTimesABC):
    uuid = models.UUIDField(unique=True, primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=255)
    description = models.TextField(default='', blank=True)
    author = models.ForeignKey('core.User', on_delete=models.PROTECT, related_name='created_companies')
    logo = models.ImageField(upload_to='companies/logos/', null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def heads(self):
        from apps.company.models import CompanyRoleABC
        return self.persons.filter(
            role__in=(
                CompanyRoleABC.CompanyRole.OWNER,
                CompanyRoleABC.CompanyRole.ADMIN,
            ),
        )

    @property
    def active_heads(self):
        return self.heads.filter(is_active=True)
