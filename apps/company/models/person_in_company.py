import uuid

from django.db import models

from mixins.models import DateTimesABC


class CompanyRoleABC(models.Model):
    class CompanyRole(models.TextChoices):
        ADMIN = 'admin'
        EMPLOYEE = 'employee'
        MANAGER = 'manager'
        OWNER = 'owner'

    role = models.CharField(max_length=100, choices=CompanyRole.choices, default=CompanyRole.EMPLOYEE)

    class Meta:
        abstract = True


class PersonInCompany(CompanyRoleABC, DateTimesABC):

    person = models.ForeignKey('core.User', on_delete=models.CASCADE, related_name='companies')
    company = models.ForeignKey('Company', on_delete=models.CASCADE, related_name='persons')
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('person', 'company')


class InvitePersonToCompany(CompanyRoleABC, DateTimesABC):
    uuid = models.UUIDField(unique=True, primary_key=True, default=uuid.uuid4)
    email = models.EmailField()
    company = models.ForeignKey('Company', on_delete=models.CASCADE, related_name='invites')
    is_active = models.BooleanField(default=True)
    date_of_joining = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = ('email', 'company')
