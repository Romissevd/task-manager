from rest_framework import permissions

from apps.company.models import PersonInCompany


class CompanyPermissions(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        person_in_company = request.user.profile.current_person_in_company
        if obj != person_in_company.company:
            return False
        return person_in_company.role in (PersonInCompany.CompanyRole.OWNER, PersonInCompany.CompanyRole.ADMIN)


class UploadCompanyLogoPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        person_in_company = request.user.profile.current_person_in_company
        return (
                request.method == 'POST' and
                person_in_company and
                person_in_company.role in (PersonInCompany.CompanyRole.OWNER, PersonInCompany.CompanyRole.ADMIN)
        )


class PersonInCompanyPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        person_in_company = request.user.profile.current_person_in_company
        if not person_in_company:
            return False
        if request.method == 'GET':
            return True
        if (
                request.method in ('PUT', 'PATCH') and
                person_in_company.role in (PersonInCompany.CompanyRole.OWNER, PersonInCompany.CompanyRole.ADMIN)
        ):
            return True
        return False


class InvitePersonToCompanyPermissions(permissions.BasePermission):

    def has_permission(self, request, view):
        person_in_company = request.user.profile.current_person_in_company
        if not person_in_company:
            return False
        return person_in_company.role in (PersonInCompany.CompanyRole.OWNER, PersonInCompany.CompanyRole.ADMIN)
