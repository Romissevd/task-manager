import uuid

from apps.company.models import InvitePersonToCompany
from apps.notification.tasks import send_email
from main import celery_app


@celery_app.task
def send_invite_to_user_to_register_with_company(invite_uuid: uuid.UUID, domain: str) -> None:
    invite = InvitePersonToCompany.objects.select_related('company').get(uuid=invite_uuid)
    full_link = f'{domain}/api/v1/accounts/registration_in_company?uid={invite.uuid}'
    send_email.delay(
        recipients=[invite.email],
        title='Invitation to the company',
        template_name='invite_to_company.html',
        data_for_template={
            'link': full_link,
            'company': invite.company.name,
        },
    )
