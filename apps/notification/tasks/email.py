from collections.abc import Sequence

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from apps.notification.models import EmailSendingHistory
from main.celery import celery_app


@celery_app.task
def send_email(
        recipients: Sequence[str],
        title: str,
        template_name: str,
        data_for_template: dict,
        from_email: str = settings.DEFAULT_FROM_EMAIL,
        backend: str = settings.DEFAULT_EMAIL_BACKEND,
):
    html_message = render_to_string(template_name, data_for_template)
    plain_message = strip_tags(html_message)
    email_status = EmailSendingHistory.Status.SUCCESS
    error_type = ''
    try:
        send_mail(
            title,
            plain_message,
            from_email,
            recipients,
            html_message=html_message,
            connection=backend,
        )
    except Exception as exc:
        email_status = EmailSendingHistory.Status.ERROR
        error_type = exc.__class__.__name__
        raise
    finally:
        EmailSendingHistory.objects.create(
            title=title,
            from_email=from_email,
            recipients=recipients,
            status=email_status,
            error_type=error_type,
        )
