from rest_framework import serializers

from apps.project.models import Project


class ProjectSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'start_date', 'end_date', 'author', 'created_at', 'updated_at')
        read_only_fields = ('author', 'created_at', 'updated_at')
