from rest_framework import routers

from apps.project.api.v1.views.project import ProjectViewSet

router = routers.SimpleRouter()
router.register(r'', ProjectViewSet, basename='project')

urlpatterns = [
]

urlpatterns += router.urls
