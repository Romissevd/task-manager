from django.db import transaction
from django.db.models import Q
from rest_framework import (
    permissions,
    viewsets,
)

from apps.project.api.v1.serializers.project import ProjectSerializer
from apps.project.models import Project
from apps.project.permissions import ProjectPermissions
from apps.project.tasks import add_company_heads_to_new_project


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (permissions.IsAuthenticated, ProjectPermissions)

    def perform_create(self, serializer):
        author = self.request.user
        with transaction.atomic():
            new_project = serializer.save(
                author=author,
                company=author.profile.current_person_in_company.company)
            new_project.memberships.create(member=author, is_active=True, author=author)
        add_company_heads_to_new_project.delay(new_project.id)

    def get_queryset(self):
        return self.queryset.filter(
            Q(author=self.request.user) |
            Q(memberships__member=self.request.user, memberships__is_active=True),
        )
