from django.db import models

from mixins.models import DateTimesABC


class Project(DateTimesABC):
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    author = models.ForeignKey('core.User', on_delete=models.PROTECT, related_name='author_projects')
    company = models.ForeignKey('company.Company', on_delete=models.CASCADE, related_name='projects')

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.name


class ProjectMember(DateTimesABC):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='memberships')
    member = models.ForeignKey('core.User', on_delete=models.CASCADE, related_name='projects')
    is_active = models.BooleanField(default=True)
    author = models.ForeignKey('core.User', on_delete=models.PROTECT, related_name='author_memberships')

    class Meta:
        unique_together = ('project', 'member')
