from django.db import models
from mptt.models import (
    MPTTModel,
    TreeForeignKey,
)

from mixins.models import (
    DateTimesABC,
    FileModelABC,
)


class Task(DateTimesABC):
    title = models.CharField(max_length=255)
    description = models.TextField(default='')
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    author = models.ForeignKey('core.User', on_delete=models.PROTECT, related_name='author_tasks')
    project = models.ForeignKey('project.Project', on_delete=models.CASCADE, related_name='tasks')
    executor = models.ForeignKey('core.User', on_delete=models.PROTECT, related_name='executor_tasks')
    status = models.ForeignKey('TaskStatus', on_delete=models.PROTECT, related_name='tasks')


class TaskStatus(models.Model):
    name = models.CharField(max_length=100, unique=True)


class TaskFile(FileModelABC):
    task = models.ForeignKey('project.Task', on_delete=models.CASCADE, related_name='files')


class TaskComment(DateTimesABC, MPTTModel):
    text = models.TextField()
    task = models.ForeignKey('project.Task', on_delete=models.CASCADE, related_name='comments', null=True, blank=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, related_name='children', null=True, blank=True)
    author = models.ForeignKey('core.User', on_delete=models.PROTECT, related_name='author_comments')
