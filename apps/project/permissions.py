from rest_framework import permissions


class ProjectPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        person_in_company = request.user.profile.current_person_in_company
        if not person_in_company:
            return False
        match request.method:
            case 'GET':
                return True
            case 'POST':
                return True
            case 'PUT', 'PATCH':
                return True
            case 'DELETE':
                return True
            case _:
                return False
