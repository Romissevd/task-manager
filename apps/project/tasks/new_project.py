from apps.core.models import User
from apps.notification.tasks import send_email
from apps.project.models import Project
from main import celery_app


@celery_app.task
def add_company_heads_to_new_project(project_id: int):
    project = Project.objects.get(id=project_id)
    company = project.company
    for head in company.active_heads:
        if head != project.author:
            project.memberships.create(member=head.person, is_active=True, author=project.author)
            notification_head_about_new_project.delay(head.person.id, project.id)


@celery_app.task
def notification_head_about_new_project(user_id: int, project_id: int):
    head = User.objects.get(id=user_id)
    project = Project.objects.get(id=project_id)
    context = {
        'project_name': project.name,
        'user_full_name': head.full_name,
        'author_full_name': project.author.full_name,
    }
    send_email(
        recipients=[head.email],
        title='New project',
        template_name='info_about_new_project.html',
        data_for_template=context,
    )
