from main.settings.contrib.celery import *  # noqa: F401, F403
from main.settings.contrib.email_backend import *  # noqa: F401, F403
from main.settings.contrib.rest_framework import *  # noqa: F401, F403
from main.settings.contrib.swagger import *  # noqa: F401, F403
