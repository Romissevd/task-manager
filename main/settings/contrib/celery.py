from main.settings.environment import env

RABBITMQ = {
    'PROTOCOL': 'amqp',
    'HOST': env.str('RABBITMQ_HOST', 'localhost'),
    'PORT': env.int('RABBITMQ_PORT', 5672),
    'USER': env.str('RABBITMQ_USER', 'guest'),
    'PASSWORD': env.str('RABBITMQ_PASSWORD', 'guest'),
}

CELERY_BROKER_URL = (
    f"{RABBITMQ['PROTOCOL']}://{RABBITMQ['USER']}:{RABBITMQ['PASSWORD']}@{RABBITMQ['HOST']}:{RABBITMQ['PORT']}"
)

CELERY_RESULT_BACKEND = 'rpc://'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
