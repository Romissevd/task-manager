import os

import certifi
from django.core.mail.backends import (
    console,
    smtp,
)

from main.settings.environment import env

if not os.environ.get('SSL_CERT_FILE'):
    os.environ['SSL_CERT_FILE'] = certifi.where()


class GoogleMailBackend(smtp.EmailBackend):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = 'smtp.gmail.com'
        self.port = 587
        self.use_tls = True
        self.username = env.str('GMAIL_USER')
        self.password = env.str('GMAIL_PASSWORD')


class ConsoleEmailBackend(console.EmailBackend):
    pass


def set_default_email_backend():
    if env.str('GMAIL_USER', '') and env.str('GMAIL_PASSWORD', ''):
        return GoogleMailBackend()
    else:
        return ConsoleEmailBackend()


DEFAULT_EMAIL_BACKEND = set_default_email_backend()
