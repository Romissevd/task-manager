REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'main.authentication.CustomTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DATE_INPUT_FORMATS': ['%Y-%m-%d', '%d.%m.%Y'],
    'DATETIME_INPUT_FORMATS': ['%Y-%m-%dT%H:%M:%S'],
    'DATE_FORMAT': '%Y-%m-%d',
    'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S',
    'DEFAULT_SCHEMA_CLASS': 'main.schemas.SwaggerSchema',
    'DEFAULT_PAGINATION_CLASS': 'generics.pagination.StandardResultsSetPagination',
}
