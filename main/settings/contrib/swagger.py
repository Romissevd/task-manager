from django.conf import settings

from main.settings.environment import env


def preprocessing_filter_spec(endpoints):
    filtered = []
    for (path, path_regex, method, callback) in endpoints:
        # Remove all but DRF API endpoints
        if settings.ADMIN_PREFIX_URL not in path:
            filtered.append((path, path_regex, method, callback))
    return filtered


SPECTACULAR_SETTINGS = {
    'TITLE': 'Task Manager API',
    'VERSION': '1.0.0',
    'SCHEMA_PATH_PREFIX': '/api/v[0-9]',
    'COMPONENT_SPLIT_REQUEST': True,
    'AUTHENTICATION_WHITELIST': [
        'main.authentication.CustomTokenAuthentication',
    ],
    'PREPROCESSING_HOOKS': [
        'main.settings.contrib.swagger.preprocessing_filter_spec',
    ],
    'SWAGGER_UI_SETTINGS': {
        'persistAuthorization': True,
    },
}

SWAGGER_URL = env.str('SWAGGER_URL', None)
