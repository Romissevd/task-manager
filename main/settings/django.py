import os
from pathlib import Path

from main.settings.environment import env

BASE_DIR = Path(__file__).resolve().parent.parent.parent


DEBUG = env.bool('DEBUG', False)

SECRET_KEY = env.str('SECRET_KEY')

ALLOWED_HOSTS = []

if DEBUG:
    ALLOWED_HOSTS.append('*')


if not DEBUG:
    CORS_ALLOWED_ORIGINS: list[str] = [
        # hosts
    ]
    CORS_ORIGIN_REGEX_WHITELIST = [
        r'http(s)?:\/\/localhost\:\d+',
    ]
else:
    CORS_ALLOW_ALL_ORIGINS = True


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Third-party apps
    'corsheaders',
    'drf_spectacular',
    'mptt',
    'rest_framework',
    'rest_framework.authtoken',
    # Local apps
    'apps.account',
    'apps.company',
    'apps.core',
    'apps.notification',
    'apps.project',
    'django_cleanup.apps.CleanupConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

if DEBUG and env.bool('ENABLE_QUERY_COUNT', False):
    MIDDLEWARE.append('querycount.middleware.QueryCountMiddleware')

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'


DATABASES = {
    'default': env.db(),
}


AUTH_USER_MODEL = 'core.User'


AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = env.str('STATIC_ROOT', os.path.join(BASE_DIR, 'static'))

MEDIA_URL = env.str('MEDIA_URL', '/media/')
MEDIA_ROOT = env.str('MEDIA_ROOT', os.path.join(BASE_DIR, 'media'))

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
