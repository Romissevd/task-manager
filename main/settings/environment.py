import os

import environ

settings_folder = environ.Path(__file__) - 1
project_root_folder = settings_folder - 2

env = environ.Env()
environ.Env.read_env(env_file=os.path.join(project_root_folder, '.env'))
