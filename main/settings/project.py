from main.settings.environment import env

ADMIN_PREFIX_URL = env.str('ADMIN_PREFIX_URL', 'admin/')

PASSWORD_MIN_LENGTH = 8
