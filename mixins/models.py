import re
from datetime import date

from django.db import models
from django.db.models import QuerySet
from django.utils import timezone


class DateTimesABC(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


def get_upload_file_path(instance, filename):
    today = date.today()
    model_name_uppercase = instance._meta.object_name
    model_name = '_'.join(re.sub(r'([A-Z])', r' \1', model_name_uppercase).split())
    return f'{today.year}/{model_name}/{today.month}/{today.day}/{filename}'


class SoftDeletionQuerySet(QuerySet):
    def delete(self):
        return super().update(deleted_at=timezone.now())

    def hard_delete(self):
        return super().delete()

    def alive(self):
        return self.filter(deleted_at=None)

    def dead(self):
        return self.exclude(deleted_at=None)


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class FileModelABC(DateTimesABC):
    file = models.FileField(upload_to=get_upload_file_path, max_length=300)
    file_type = models.CharField(max_length=100)
    deleted_at = models.DateTimeField(null=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True


class AuthorABC(models.Model):
    author = models.ForeignKey('core.User', on_delete=models.SET_NULL, null=True, related_name='%(class)ss')

    class Meta:
        abstract = True
